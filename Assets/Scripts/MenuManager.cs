﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    private void Start()
    {
        Game.wallet = 0;
    }

    public void PlayGame() {
        SceneManager.LoadScene("Levels");
    }

    public void GoCars()
    {
        SceneManager.LoadScene("Cars");
    }


    public void ExitGame()
    {
        Application.Quit();
    }

    public void GoMenu()
    {
        SceneManager.LoadScene("Menu");
        Game.wallet = 0;
    }
}
