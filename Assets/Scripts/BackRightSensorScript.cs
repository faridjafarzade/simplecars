﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackRightSensorScript : MonoBehaviour {

    public GameObject car;

    private Vector3 offset;

    void Start()
    {
        offset = transform.position - car.transform.position;
    }

    void LateUpdate()
    {
        transform.position = car.transform.position + offset;
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("outway"))
        {
            SensorManager.backRightGo = false;

        }
        else
        {

            SensorManager.backRightGo = true;

            if (other.gameObject.CompareTag("finish"))
            {
                SensorManager.finish = true;

            }

        }
    }
}
