﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontSensorScript : MonoBehaviour {

    public GameObject car;

    private Vector3 offset;
    private bool state = true;
    int wait = 0;

    void Start()
    {
        offset = transform.position - car.transform.position;
    }

    void LateUpdate()
    {
        transform.position = car.transform.position + offset;
        SensorManager.frontX = transform.position.x;
        SensorManager.frontZ = transform.position.z;
    }



    void OnTriggerEnter(Collider other)
    {


        if (other.gameObject.CompareTag("outway"))
        {
            SensorManager.frontGo = false;
        }
        else SensorManager.frontGo = true;

    }


    void Update()
    {

        //SensorManager.backGo = state;
    }


}
