﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelButton {

    private string text;
    private Level level;
    public static int order = 0;


    private void clickHandler() {
        Game.level = this.level;
        SceneManager.LoadScene("MiniGame");
    }

     public  LevelButton(Level level,Canvas canvas,Button button){

        this.level = level;
        Button butt1 = GameObject.Instantiate(button, new Vector3(30, 30, 0), Quaternion.identity);
        butt1.transform.SetParent(canvas.transform, false);
        int k = 300 - (50 * order);
        
        butt1.transform.position = new Vector3(500,k , 0);
        butt1.GetComponentInChildren<Text>().text = "Open " + this.level.name ;

        order++;
        butt1.onClick.AddListener(clickHandler);
    }

}
