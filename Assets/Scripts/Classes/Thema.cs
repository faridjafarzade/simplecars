﻿using System.Collections.Generic;
using UnityEngine;

public class GameThema {

    public List<GameObject> sideObjects { get; set; }
    public List<GameObject> wayObjects { get; set; }
    public string name { get; set; }
    public string way { get; set; }
    public GameObject ground { get; set; }
    public Material finishMaterial { get; set; }

    public GameThema(string name , List<GameObject> wayObjects, List<GameObject> sideObjects, GameObject ground , Material finishMaterial,string way) {

        this.name = name;
        this.wayObjects = wayObjects;
        this.sideObjects = sideObjects;
        this.ground = ground;
        this.finishMaterial = finishMaterial;
        this.way = way;

    }

    public GameThema(string name, List<string> wayObjects, List<string> sideObjects, string ground, Material finishMaterial)
    {

        this.name = name;
        foreach (var wo in wayObjects)
        {
            this.sideObjects.Add(GameObject.FindGameObjectWithTag(wo));
        }

        foreach (var so in sideObjects)
        {
            this.sideObjects.Add(GameObject.FindGameObjectWithTag(so));
        }

        this.ground = GameObject.FindGameObjectWithTag(ground);
        this.finishMaterial = finishMaterial;

    }


}
