﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car {

   public  GameObject gm;

    public double speed ;
    public double maxSpeed;
    public double rightSpeed;
    public double backSpeed;
    public double leftSpeed;
    public double postponement;
    public int id;
    public string name;

    public Car(string name,int id,GameObject gm, double maxSpeed ,  double postponement)
    {
        this.id = id;
        this.gm = gm;
        this.maxSpeed = maxSpeed;
        this.postponement = postponement;
        this.name = name;

    }

    public Car(string name, int id,  double maxSpeed, double postponement)
    {
        this.id = id;
        this.maxSpeed = maxSpeed;
        this.postponement = postponement;
        this.name = name;

    }


}
