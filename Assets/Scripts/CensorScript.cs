﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CensorScript : MonoBehaviour {

    public GameObject car;

    public static string state = "";

    private Vector3 offset;

    void Start()
    {
        offset = transform.position - car.transform.position;
    }

    void LateUpdate()
    {
        transform.position = car.transform.position + offset;
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("outway"))
        {
            CensorScript.state = "outway";
           
        }
        else if (other.gameObject.CompareTag("finish"))
        {
            CensorScript.state = "finish";

        }
    }

}
