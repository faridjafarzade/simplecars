﻿using UnityEngine;

public class CarMovementScript : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {


        if (other.gameObject.CompareTag("coin"))
        {
            Game.wallet ++;
            SetCountText();
            other.gameObject.SetActive(false);
        }

    }



    void SetCountText()
    {
        Debug.Log("wallet   "+Game.wallet);
        Game.staticCountText.text = "Count: " + Game.wallet;

    }

}