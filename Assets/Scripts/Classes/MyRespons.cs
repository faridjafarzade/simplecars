﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class MyRespons{

    public bool error;
    public bool success;
    public int http_status_codes;
    public User user;
    public string api;

    public static MyRespons CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<MyRespons>(jsonString);
    }

}
