﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarPanel{

    
    private Car car;
    public static int order = 0;
    Text selected;

    private void clickHandler()
    {
        
        Game.car = this.car.id;
        CarsPage.unselectAll();
        this.Select();
    }

    public CarPanel(Car car, Canvas canvas, Button button,Text text)
    {

        this.car = car;

        Button butt1 = GameObject.Instantiate(button, new Vector3(30, 30, 0), Quaternion.identity);
        Text name = GameObject.Instantiate(text, new Vector3(30, 30, 0), Quaternion.identity);
        selected = GameObject.Instantiate(text, new Vector3(30, 30, 0), Quaternion.identity);

        butt1.transform.SetParent(canvas.transform, false);
        name.transform.SetParent(canvas.transform, false);
        selected.transform.SetParent(canvas.transform, false);
        int k = getPosition();

        name.transform.position = new Vector3(500, k, 0);
        name.text = "Name: " + car.name+ "Max Speed: " + car.maxSpeed + " Postponoment: " + car.postponement;
        
        order++;

       k = getPosition();

        selected.transform.position = new Vector3(500, k, 0);
        if (car.id == Game.car)
            selected.text = "selected";
        else 
            selected.text = "";

        order++;

        k = getPosition();

        butt1.transform.position = new Vector3(500, k, 0);
        butt1.GetComponentInChildren<Text>().text = "Select ";

        order++;



        butt1.onClick.AddListener(clickHandler);
    }

    public void Select()
    {
        selected.text = "Selected";
    }

    public int getPosition()
    {
        return 350 - (30 * order); 
    }

    public void Unselect()
    {
        selected.text = "";
    }

}
