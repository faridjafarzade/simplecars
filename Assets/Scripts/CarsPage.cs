﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CarsPage : MonoBehaviour {

    private static List<CarPanel> panels = new List<CarPanel>();
    public Text text;
    public Button button;
    public Canvas canvas;

    void Start()
    {
        //CarManager.desingCars();

        //foreach (var car in CarManager.cars)
        //{
           // Debug.Log("car " + car.name);
           // panels.Add(new CarPanel(car, canvas, button, text));
        //}

        //CarPanel.order = 0;

    }


    // Update is called once per frame
    public static void unselectAll () {
        foreach (var panel in panels)
        {
            panel.Unselect();
        }
    }

    public void SelectSkyBus()
    {
        Game.car = 1;
        SceneManager.LoadScene("Menu");
    }

    public void SelectDirtyBlue()
    {
        Game.car = 0;
        SceneManager.LoadScene("Menu");
    }
}
