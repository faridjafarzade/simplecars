﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level {

    public int maxLength { get; set; }
    public int requiredBranch { get; set; }
    public int requiredLength { get; set; }

    public string name { get; set; }

    public float requiredMapTime { get; set; }
    public float mapTime { get; set; }
    public float time { get; set; }
    public int thema { get; set; }

    public List<string> advantageCars { get; set; }
    public List<string> disadvantageCars { get; set; }
    public List<string> requiredCars { get; set; }
    public List<string> bannedCars { get; set; }

    public Level(int maxLength, int requiredBranch, int requiredLength, int time, int requiredMapTime, int mapTime, int thema, List<string> advantageCars, List<string> disadvantageCars, List<string> requiredCars, List<string> bannedCars)
    {
        this.maxLength = maxLength;
        this.requiredBranch = requiredBranch;
        this.requiredLength = requiredLength;
        this.requiredMapTime = requiredMapTime;
        this.mapTime = mapTime;
        this.time = time;
        this.thema = thema;
        this.advantageCars = advantageCars;
        this.disadvantageCars = disadvantageCars;
        this.requiredCars = requiredCars;
        this.bannedCars = bannedCars;
    }

    public Level(string name,int maxLength, int requiredBranch, int requiredLength, int time, int requiredMapTime, int mapTime, int thema, List<string> advantageCars, List<string> disadvantageCars, List<string> requiredCars, List<string> bannedCars)
    {
        this.maxLength = maxLength;
        this.requiredBranch = requiredBranch;
        this.requiredLength = requiredLength;
        this.requiredMapTime = requiredMapTime;
        this.mapTime = mapTime;
        this.time = time;
        this.thema = thema;
        this.advantageCars = advantageCars;
        this.disadvantageCars = disadvantageCars;
        this.requiredCars = requiredCars;
        this.bannedCars = bannedCars;
        this.name = name;
    }

}
