﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class Way
{
    public List<WayPart> parts = new List<WayPart>();

    public int maxLength { get; set; }
    public int requiredBranch { get; set; }
    public int requiredLength { get; set; }
    public int width { get; set; }
    public float x { get; set; }
    public float z { get; set; }
    public int stepSize { get; set; }
    public GameObject ground { get; set; }

    //public static WayObject[,] cordinates{ get; set; }
    //it is working with vcordinat systems like  cordinates[x+" "+z]  
    public static Dictionary<string, WayObject> cordinates = new Dictionary<string, WayObject>();

    public static List<WayObject> openBranchs = new List<WayObject>();

    public static List<GameObject> sideObjects = new List<GameObject>();
    public List<GameObject> wayObjects = new List<GameObject>();

    public Way(Level currentLevel, List<GameObject> wayObjects, List<GameObject> sideObjects, GameObject ground)
    {
        this.maxLength = currentLevel.maxLength;
        this.wayObjects = wayObjects;
        Way.sideObjects = sideObjects;
        this.ground = ground;

        this.width = 1;
        this.x = 0;
        this.stepSize = 10;

        this.requiredBranch = 3;

        this.requiredLength = 20;

        this.stepSize = 10;
        this.z = Random.Range(0, this.maxLength);
        this.z = 0;


    }

        public Way(int maxLength, List<GameObject> wayObjects, List<GameObject> sideObjects, GameObject ground)
    {
        this.maxLength = maxLength;
        this.wayObjects = wayObjects;
        Way.sideObjects = sideObjects;
        this.ground = ground;

        this.width = 1;
        this.x = 0;
        this.stepSize = 10;

        this.requiredBranch = 3;

        this.requiredLength = 20;

        this.stepSize = 10;
        this.z = Random.Range(0, this.maxLength);
        this.z = 0;

    }

    public Way(int maxLength)
    {
        this.maxLength = maxLength;

        this.width = 1;
        this.x = 0;
        this.stepSize = 10;

        this.requiredBranch = 3;

        this.requiredLength = 20;

        this.stepSize = 10;
        this.z = Random.Range(0, this.maxLength);
        this.z = 0;

    }


    public Way(Level currentLevel)
    {
        this.maxLength = currentLevel.maxLength;

        this.width = 1;
        this.x = 0;
        this.stepSize = 10;

        this.requiredBranch = 3;

        this.requiredLength = 20;

        this.stepSize = 10;
        this.z = Random.Range(0, this.maxLength);
        this.z = 0;

    }
    public void GenerateWay()
    {
        float l = this.maxLength + this.z;
        bool started = false;
        while (0 < this.maxLength)
        {
            int length = Random.Range(5, this.maxLength);
            this.maxLength = this.maxLength - length;
            int dr = Random.Range(0, 2);

            if (dr == 0 || !started) this.GenerateUpWay(length);
            else this.GenerateDownWay(length);

            length = Random.Range(5, this.maxLength);
            this.maxLength = this.maxLength - length;
            dr = Random.Range(0, 2);

            if (dr == 0) this.GenerateRightWay(length);
            else this.GenerateLeftWay(length);
            started = true;
        }
        float finishX = this.x;
        float finishZ = this.z;
        addBranch();

        List<WayObject> lwo = new List<WayObject>();
        foreach (var pair in Way.cordinates)
        {
            lwo.Add(pair.Value);
        }

        foreach (var pair in Way.cordinates)
        {
            string key = pair.Key;
            WayObject wo = pair.Value;
            wo.generate();
        }
        WayObject lw = lwo[lwo.Count - 1];
        WayObjectManager.addFinish(Way.cordinates[lw.x + "-" + lw.z]);
    }

    public static bool inWay(int x,int z)
    {
        int kx = (x - x % 10) / 10;
        int kz = (z - z % 10) / 10;

        string key = kx + "-" + kz;
        CarScript.myPrint("inway ----- "+key);
        return Way.cordinates.ContainsKey(key);
    }

    public void addBranch()
    {

        List<WayObject> lwo = new List<WayObject>();
        foreach (var pair in Way.cordinates)
        {
            lwo.Add(pair.Value);
        }

        int number = Random.Range(0, lwo.Count);

        WayObject wo = lwo[number];

        float x = wo.x;
        float z = wo.z;

        int length = Random.Range(5, this.requiredLength);
        length = length > 6 ? length : 11;

       
        this.requiredLength = this.requiredLength - length;
        this.requiredLength = this.requiredLength > 0 ? this.requiredLength : 0;
        length = length > 5 ? length : Random.Range(5, 25);
        int dr = Random.Range(0, 2);
        int ty = Random.Range(0, 2);
        if (ty == 1)
        {
            if (wo.wayDirections.IndexOf("v") < 0)
            {
                if (dr == 0) this.AddUpWay(length, x, z);
                else this.AddDownWay(length, x, z);
            }
            else
            {
                if (dr == 0) this.AddRightWay(length, x, z);
                else this.AddLeftWay(length, x, z);
            }
        }
        else
        {
            if (wo.wayDirections.IndexOf("h") < 0)
            {
                if (dr == 0) this.AddRightWay(length, x, z);
                else this.AddLeftWay(length, x, z);
            }
            else
            {
                if (dr == 0) this.AddUpWay(length, x, z);
                else this.AddDownWay(length, x, z);
            }
        }


        this.requiredBranch--;

        if (requiredBranch > 0 || requiredLength > 0) this.addBranch();

    }

    public void GenerateRightWay(int length)
    {
        GameObject wo = ThemaManager.currentThema.wayObjects[Random.Range(0, ThemaManager.currentThema.wayObjects.Count)];

        for (int i = 0; i < length; i++)
        {

            HorizontalWayPart hwp = new HorizontalWayPart(wo, this.width, this.x, this.z, this.stepSize, "h");
            this.x = this.x + 1;
            MapGenerator.setCordinates(x, z);

        }
    }

    public void AddRightWay(int length, float x, float z)
    {


        GameObject wo = ThemaManager.currentThema.wayObjects[Random.Range(0, ThemaManager.currentThema.wayObjects.Count)];

        for (int i = 0; i < length; i++)
        {
            HorizontalWayPart hwp = new HorizontalWayPart(wo, this.width, x, z, this.stepSize, "h");
            x = x + 1;
            MapGenerator.setCordinates(x, z);

        }
    }

    public void GenerateLeftWay(int length)
    {

        GameObject wo = ThemaManager.currentThema.wayObjects[Random.Range(0, ThemaManager.currentThema.wayObjects.Count)];

        for (int i = 0; i < length; i++)
        {
            HorizontalWayPart hwp = new HorizontalWayPart(wo, this.width, this.x, this.z, this.stepSize, "h");
            this.x = this.x - 1;
            MapGenerator.setCordinates(x, z);

        }


    }

    public void AddLeftWay(int length, float x, float z)
    {

        GameObject wo = ThemaManager.currentThema.wayObjects[Random.Range(0, ThemaManager.currentThema.wayObjects.Count)];




        for (int i = 0; i < length; i++)
        {
            HorizontalWayPart hwp = new HorizontalWayPart(wo, this.width, x, z, this.stepSize, "h");
            x = x - 1;
            MapGenerator.setCordinates(x, z);

        }

    }

    public void GenerateUpWay(int length)
    {

        GameObject wo = ThemaManager.currentThema.wayObjects[Random.Range(0, ThemaManager.currentThema.wayObjects.Count)];



        for (int i = 0; i < length; i++)
        {
            VerticalWayPart vwp = new VerticalWayPart(wo, this.width, this.x, this.z, this.stepSize, "v");
            this.z = this.z + 1;
            MapGenerator.setCordinates(x, z);

        }

    }

    public void AddUpWay(int length, float x, float z)
    {


        GameObject wo = ThemaManager.currentThema.wayObjects[Random.Range(0, ThemaManager.currentThema.wayObjects.Count)];



        for (int i = 0; i < length; i++)
        {
            VerticalWayPart vwp = new VerticalWayPart(wo, this.width, x, z, this.stepSize, "v");
            z = z + 1;
            MapGenerator.setCordinates(x, z);

        }

    }

    public void GenerateDownWay(int length)
    {

        GameObject wo = ThemaManager.currentThema.wayObjects[Random.Range(0, ThemaManager.currentThema.wayObjects.Count)];
        for (int i = 0; i < length; i++)
        {
            VerticalWayPart vwp = new VerticalWayPart(wo, this.width, this.x, this.z, this.stepSize, "v");
            this.z = this.z - 1;
            MapGenerator.setCordinates(x, z);
        }
    }

    public void AddDownWay(int length, float x, float z)
    {

        GameObject wo = ThemaManager.currentThema.wayObjects[Random.Range(0, ThemaManager.currentThema.wayObjects.Count)];

        for (int i = 0; i < length; i++)
        {
            VerticalWayPart vwp = new VerticalWayPart(wo, this.width, x, z, this.stepSize, "v");
            z = z - 1;
            MapGenerator.setCordinates(x, z);
        }
    }

}
