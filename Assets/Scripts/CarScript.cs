﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class CarScript : MonoBehaviour
{

    Rigidbody m_Rigidbody;
    double y = 0.0f;
    double x = 0.0f;
    double z = 0.0f;
   

    public Text countText;
    public int coinCount= 0;
    public Text winText;
    public Text timerText;
    public Text mapTimerText;
    public float targetTime = 5.0f;

    public Camera currentCamera;
    public Camera rightCamera;
    public Camera leftCamera;
    public Camera frontCamera;
    public Camera backCamera;
    public Camera mapCamera;

    public GameObject mapObject;

    public Button rightButton;
    public Button leftButton;
    public Button upButton;
    public Button downButton;

    public Button activateMapButton;
    public Button deactivateMapButton;

    public Button rightButtonMap;
    public Button leftButtonMap;
    public Button upButtonMap;
    public Button downButtonMap;
    public Button zoomInButtonMap;
    public Button zoomOutButtonMap;

    public bool turnRight = false;
    public bool turnLeft = false;
    public bool moveForward = false;
    public bool moveBack = false;



    public GameObject blucar;
    public GameObject bus;
    public List<GameObject> cars =new List<GameObject>();


    public float requiredMapTime ;
    float mapTime;
    float time;

    public bool mapActive = false;

    public string[,] cordinates;
    //public GameObject car;

    public Way way;

    public static void myPrint(string text)
    {
        //print(text);
    }

    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        Game.staticCountText = countText;

        rightCamera.enabled = false;
        leftCamera.enabled = false;
        frontCamera.enabled = false;
        backCamera.enabled = true;

        currentCamera = backCamera;
        mapCamera.enabled = true;

        activateMapButton.onClick.AddListener(activateMap);

        deactivateMapButton.onClick.AddListener(deactivateMap);
        //rightButton.OnPointerDown.AddListener(turnRight);
        winText.text = "";

       

        Game.staticCountText.text = "Coin : " + Game.wallet;



        blucar.SetActive(false);
        bus.SetActive(false);

        cars.Add(blucar);
        cars.Add(bus);

        CarManager.desingCars();
        CarManager.setCar(cars[Game.car]);



        CarManager.changePosition(x, z);
        
            
        
        this.x = 0;

        this.y = -2;

        this.z = 0;
        transform.localPosition = new Vector3((int) this.x, (int)this.y, (int) this.z);


        //CarManager.activeCar.SetActive(true);
        //print(this.way.x + " x--------------------z " + CarManager.activeCar.active);
        //CarManager.activeCar.transform.localPosition = new Vector3((int)this.x, -2, (int)this.z);
        // mapCamera =  MapGenerator.generate(mapCamera);
        //mapObject.transform.localPosition = mapObject.transform.localPosition + MapGenerator.generate();
        requiredMapTime = Game.level.requiredMapTime;
        time = Game.level.time;
        mapTime = Game.level.mapTime;

        turnMap(true);
    }


    void FixeduUpdate()
    {
        if (Input.GetKey(KeyCode.UpArrow))
            transform.Translate(Vector3.forward * 1 * Time.deltaTime);

        if (Input.GetKey(KeyCode.DownArrow))
            transform.Translate(-Vector3.forward * 1 * Time.deltaTime);

        if (Input.GetKey(KeyCode.LeftArrow))
            transform.Rotate(Vector3.up, -2 * Time.deltaTime);

        if (Input.GetKey(KeyCode.RightArrow))
            transform.Rotate(Vector3.up, 2 * Time.deltaTime);
    }





    void finish()
    {


        SceneManager.LoadScene("WinScreen");
    }

    void gameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    void Update() {
    
        
        if (requiredMapTime <= 0) {
                        turnMap(false);
            if (mapTime<=0)
            {
                activateMapButton.gameObject.SetActive(false);
            }

            time -= Time.deltaTime;
                    int t = (int)Mathf.Round(time);
                    timerText.text = "Time : " + t;
                    if (time <= 0)
                    {
                        gameOver();
                    }
        }
        else
        {
            requiredMapTime -=  Time.deltaTime;
            int t = (int)Mathf.Round(requiredMapTime);
            mapTimerText.text = "Time : " + t;
            if (mapActive)
            {
               mapTime -= Time.deltaTime;
            }

        }
    }
    float currentRotation = 0;
        void FixedUpdate()
    {

        


        //if ((Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.UpArrow)) && Input.GetKey(KeyCode.RightArrow))
        if (Input.GetKey(KeyCode.RightArrow)||turnRight)
        {


            //transform.localPosition = new Vector3(x, y, z);
            currentRotation = currentRotation + 90;
            if (CarManager.activeCar_.rightSpeed < CarManager.activeCar_.maxSpeed)
                CarManager.activeCar_.rightSpeed = CarManager.activeCar_.rightSpeed + CarManager.activeCar_.postponement;
            //transform.eulerAngles = new Vector3(0, 90, 0);
            CarManager.rotate(90);

            //transform.Rotate(0, (float)rotate, 0);
            //CarManager.rotate(this.gameObject, 90);
            //turnOnRightCamera();

        }

        if (Input.GetKey(KeyCode.LeftArrow) || turnLeft)
        {
            //x = x - 10;

            if (CarManager.activeCar_.leftSpeed < CarManager.activeCar_.maxSpeed)
                CarManager.activeCar_.leftSpeed = CarManager.activeCar_.leftSpeed + CarManager.activeCar_.postponement;
            //    transform.eulerAngles = new Vector3(0, 270, 0);
            CarManager.rotate(270);
            //turnOnLeftCamera();

        }

        if (Input.GetKey(KeyCode.UpArrow) || moveForward)
        {

            if (CarManager.activeCar_.speed < CarManager.activeCar_.maxSpeed)
                CarManager.activeCar_.speed = CarManager.activeCar_.speed + CarManager.activeCar_.postponement;
            CarManager.rotate(0);
            //turnOnBackCamera();
        }

        if (Input.GetKey(KeyCode.DownArrow) || moveBack)
        {
            if (CarManager.activeCar_.backSpeed < CarManager.activeCar_.maxSpeed)
                CarManager.activeCar_.backSpeed = CarManager.activeCar_.backSpeed + CarManager.activeCar_.postponement;
            CarManager.rotate(180);
            //turnOnFrontCamera();
        }

        if (CarManager.activeCar_.leftSpeed > 0)
            CarManager.activeCar_.leftSpeed = CarManager.activeCar_.leftSpeed - 0.1;
        else CarManager.activeCar_.leftSpeed = 0;

        if (CarManager.activeCar_.rightSpeed > 0)
            CarManager.activeCar_.rightSpeed = CarManager.activeCar_.rightSpeed - 0.1;
        else CarManager.activeCar_.rightSpeed = 0;

        if (CarManager.activeCar_.speed > 0)
            CarManager.activeCar_.speed = CarManager.activeCar_.speed - 0.1;
        else CarManager.activeCar_.speed = 0;

        if (CarManager.activeCar_.backSpeed > 0)
            CarManager.activeCar_.backSpeed = CarManager.activeCar_.backSpeed - 0.1;
        else CarManager.activeCar_.backSpeed = 0;

        double rotate = CarManager.activeCar_.rightSpeed - CarManager.activeCar_.leftSpeed;

        //Quaternion turnRotation = Quaternion.Euler(0f, (float)rotate, 0f);

        // Apply this rotation to the rigidbody's rotation.
        //m_Rigidbody.MoveRotation(m_Rigidbody.rotation * turnRotation);


        
        //transform.Rotate(Vector3.down, (float)rotate);
        //transform.Rotate(new Vector3(0f, (float)rotate, 0f));
        //transform.RotateAround(transform.localPosition, transform.up, (float)rotate);
        //transform.RotateAround(transform.position, Vector3.right , (float)rightRotationSpeed*50 * Time.deltaTime);
       
        //print("direction " + d);
        //if (d > 270) turnOnLeftCamera();
        //else if (d > 180) turnOnFrontCamera();
        //else if (d > 90) turnOnRightCamera();
        //else turnOnBackCamera();




        if (Way.inWay((int)(x + rotate), (int)z))
            x = x + (float)rotate;

        ///x = x + (float)rotate;

        //Vector3 movement = transform.rotation * Vector3.forward;
        //if (leftRotationSpeed != 0 || rightRotationSpeed != 0||backSpeed != 0 || speed != 0)
        //transform.localPosition = new Vector3((float)x, (float)y, (float)z);

        //m_Rigidbody.velocity = movement * (float)speed * 10;
        // if (backSpeed != 0 || speed != 0)


        if (Way.inWay((int)x, (int)(z+ CarManager.activeCar_.speed)))
                    z = z + CarManager.activeCar_.speed;
                if (Way.inWay((int)x, (int)(z - CarManager.activeCar_.backSpeed)))
                    z = z - CarManager.activeCar_.backSpeed;

                
                // CarManager.rotate(rotate);
                transform.localPosition = new Vector3((float)x, (float)y, (float)z);
                CarManager.changePosition(x, z);


        //transform.localPosition = transform.rotation * new Vector3((float)x, (float)y, (float)z);
    }

    public void moveForwardUp(){ moveForward = false ; }
    public void moveForwardDown() { moveForward = true; }
    public void moveBackUp() { moveBack = false; }
    public void moveBackDown() { moveBack = true; }
    public void turnRightUp() { turnRight = false; }
    public void turnRightDown() { turnRight = true; }
    public void turnLeftUp() { turnLeft = false; }
    public void turnLeftDown() { turnLeft = true; }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("finish"))
        {
            finish();
        }
        print(other.gameObject.tag);
    }

   
    void turnOffCameras()
    {
        rightCamera.enabled = false;
        leftCamera.enabled = false;
        frontCamera.enabled = false;
        backCamera.enabled = false;

    }
    void turnOnFrontCamera()
    {
        turnOffCameras();
        currentCamera = frontCamera;
        currentCamera.enabled = true;

    }
    void turnOnBackCamera()
    {
        turnOffCameras();
        currentCamera = backCamera;
        currentCamera.enabled = true;

    }
    void turnOnRightCamera()
    {
        turnOffCameras();
        currentCamera = rightCamera;
        currentCamera.enabled = true;

    }
    void turnOnLeftCamera()
    {
        turnOffCameras();
        currentCamera = leftCamera;
        currentCamera.enabled = true;

    }

   public void turnMap(bool map)
    {
        rightButton.gameObject.SetActive(!map);
        leftButton.gameObject.SetActive(!map);
        upButton.gameObject.SetActive(!map);
        activateMapButton.gameObject.SetActive(!map);
        downButton.gameObject.SetActive(!map);
        currentCamera.enabled = !map;
        timerText.gameObject.SetActive(!map);
        winText.gameObject.SetActive(!map);
        countText.gameObject.SetActive(!map);

        rightButtonMap.gameObject.SetActive(map);
        leftButtonMap.gameObject.SetActive(map);
        upButtonMap.gameObject.SetActive(map);
        downButtonMap.gameObject.SetActive(map);
        zoomInButtonMap.gameObject.SetActive(map);
        zoomOutButtonMap.gameObject.SetActive(map);
        deactivateMapButton.gameObject.SetActive(map);
        mapCamera.enabled = map;
        mapTimerText.gameObject.SetActive(map);
    }

    public void activateMap() {
        requiredMapTime = mapTime;
        print(" cr " + requiredMapTime);

        mapActive = true;
        turnMap(true);
    }

    public void deactivateMap()
    {
       // mapTime = requiredMapTime;
        requiredMapTime = 0;
    }

    }
