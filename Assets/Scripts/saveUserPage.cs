﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class saveUserPage : MonoBehaviour
{

    public Text loadingText;

    IEnumerator Start()
    {
        
        DBAccess db = new DBAccess();
        UnityWebRequest download = db.saveUser();

        yield return download.SendWebRequest();
        if (download.isNetworkError || download.isHttpError)
        {
            Debug.Log("Error : " + download.error);
        }
        else
        {


            Debug.Log(download.downloadHandler.text);

            SceneManager.LoadScene("Levels");
        }
    }


}
