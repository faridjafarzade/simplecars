﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackSensorScript : MonoBehaviour {

    public GameObject car;

    private Vector3 offset;
    private bool state = true;
    int wait = 0;

    void Start()
    {
        offset = transform.position - car.transform.position;
    }

    void LateUpdate()
    {
        transform.position = car.transform.position + offset;
        SensorManager.backX = transform.position.x;
        SensorManager.backZ = transform.position.z;
    }

    private void FixedUpdate()
    {
        
    }



    void OnTriggerStay(Collider other)
    {

       
        if (other.gameObject.CompareTag("outway"))
        {
            SensorManager.backGo = false;
        }
        else SensorManager.backGo = true;
    }
    

    void Update()
    {
      
        //SensorManager.backGo = state;
    }

    
}
