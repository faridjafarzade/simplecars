﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
public class DBAccess
{
    
    private string url = "www.maskeddream.com/cargame/apis/user_api.php";

    public DBAccess()
    {
        
    }

    public IEnumerator saveddUser(User user) {

        WWWForm form = new WWWForm();
        form.AddField("user_name", user.userName);
        form.AddField("coin_count", user.coinCount);
        form.AddField("passed_level", user.passedLevel);
        form.AddField("experience", user.experience);
        form.AddField("action", "update");

        var download = UnityWebRequest.Post("http://url"+url+"?action=get_user&user_id=1", form);
        download.SetRequestHeader("action", "update");
        download.method = "POST";

        // Wait until the download is done
        yield return download.Send();

        if (download.isNetworkError || download.isHttpError)
        {
            
        }
        else
        {
            
            
        }
    }


    public UnityWebRequest saveUser()
    {

        WWWForm form = new WWWForm();

        form.AddField("user_id", "1");
        form.AddField("action", "get_user");

        string u = url + "?action=update&user_id=1&coin_count="+Game.user.coinCount+ "&passed_level=" + Game.user.passedLevel + "&username=" + Game.user.userName;
        var download = UnityWebRequest.Post(u, form);
        download.SetRequestHeader("action", "update");
        Debug.Log("sended " + u);
        return download;
    }

    public UnityWebRequest getUser()
    {

        WWWForm form = new WWWForm();
       // form.AddField("user_id", "1");
        //form.AddField("action", "get_user");

        string u = url + "?action=get_user&user_id=1";
        var download = UnityWebRequest.Post(u , form);
        //download.SetRequestHeader("action", "?action=get_user");

        return download;
    }
}