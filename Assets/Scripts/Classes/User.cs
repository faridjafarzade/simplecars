﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class User{

    public string id;
    public string userName;
    public string experience;
    public string startDate;
    public string coinCount;
    public string passedLevel;
    public int[] cars;

  

    public static User CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<User>(jsonString);
    }
}
