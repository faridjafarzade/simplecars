﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using UnityEngine.SceneManagement;

public class LevelsPage : MonoBehaviour {

    public Button button;
    public Canvas canvas;

    void Start () {
        LevelManager.designLevels();

        //foreach (var level in LevelManager.levels)
        //{
            //LevelButton lb = new LevelButton(level, canvas, button);
        //}

        //LevelButton.order = 0;
        
    }

    public void GoLongDesert()
    {
        Game.level = LevelManager.levels[1];
        SceneManager.LoadScene("MiniGame");
    }

    public void IcecreamLand()
    {
        Game.level = LevelManager.levels[0];
        SceneManager.LoadScene("MiniGame");
    }

    void Update () {
		
	}
}
