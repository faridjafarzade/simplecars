﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserCar{

    public int maxSpeed;
    public int maxBackSpeed;
    public int carId;
    public int rotationSpeed;
    public int postponement;
    public int skin;

    public UserCar(int maxSpeed, int maxBackSpeed, int carId, int rotationSpeed, int postponement, int skin)
    {
        this.maxSpeed = maxSpeed;
        this.maxBackSpeed = maxBackSpeed;
        this.carId = carId;
        this.rotationSpeed = rotationSpeed;
        this.postponement = postponement;
        this.skin = skin;
    }

    public UserCar(int carId)
    {
        this.carId = carId;
        this.maxSpeed = 0;
        this.maxBackSpeed = 0;
        this.rotationSpeed = 0;
        this.postponement = 0;
    }


}
