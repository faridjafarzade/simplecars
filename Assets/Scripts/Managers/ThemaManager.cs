﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThemaManager : MonoBehaviour {


    public GameObject purpleTree;
    public GameObject purpleTree2;
    public GameObject purpleTree3;
    public GameObject coin;
    public static GameObject staticCoin;
    public GameObject purpleWay;
    public GameObject purpleGround;
    public Camera mapCamera;
    public Skybox purpleSky;
    public Material purpleFinishMaterial;


    public GameObject blueWay;
    public GameObject blueGround;
    public GameObject blueBuilding;
    public GameObject blueTree;
    public Skybox blueSky;
    public Material blueFinishMaterial;
    public List<GameObject> blueWayObjects = new List<GameObject>();
    public List<GameObject> blueSideObjects = new List<GameObject>();


    public List<GameObject> purpleWayObjects = new List<GameObject>();
    public List<GameObject> purpleSideObjects = new List<GameObject>();
    public List<GameThema> themas = new List<GameThema>();
    public static GameThema currentThema;

    void Start () {
        staticCoin = coin;

        purpleWayObjects.Add(purpleWay);
        purpleSideObjects.Add(purpleTree);
        purpleSideObjects.Add(purpleTree2);
        purpleSideObjects.Add(purpleTree3);
        GameThema purpleThema = new GameThema("purpleThema", purpleWayObjects, purpleSideObjects, purpleGround, purpleFinishMaterial,"asphalt");
          themas.Add(purpleThema);

        blueWayObjects.Add(blueWay);
        blueSideObjects.Add(blueBuilding);
        blueSideObjects.Add(blueTree);
        GameThema blueThema = new GameThema("blueThema", blueWayObjects, blueSideObjects, blueGround, blueFinishMaterial,"sandway");
        themas.Add(blueThema);

        Way way = new Way(Game.level);
          currentThema = themas[Game.level.thema];
          way.GenerateWay();

        mapCamera = MapGenerator.generate(mapCamera);
    }


}
