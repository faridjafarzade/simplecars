﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UserController : MonoBehaviour {

    public float speed = 15f; // let's set a decent speed as default
    private Rigidbody rb;

    float moveHorizontal = 0;
    float moveVertical = 0;

    public Text countText;
    public Text winText;
    private int count;

    void Start() // Upper case because in C# casing counts!
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    void FixedUpdate()
    {
        moveHorizontal = Input.GetAxis("Horizontal");
        moveVertical  = Input.GetAxis("Vertical");

        print(moveHorizontal);

        

        // Let's assign both x and z
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
       
        rb.AddForce(movement);
    }



    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 8)
        {
            winText.text = "You Win!";
        }
    }
}