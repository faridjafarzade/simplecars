﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WayPart
{


    

    public int width { get; set; }
    public int stepSize { get; set; }

    public float x { get; set; }
    public float z { get; set; }

    public WayPart(GameObject way, int width, float x, float z, int stepSize, string direction)
    {
        this.x = x;
        this.z = z;

        string key = this.x + "-" + this.z;

        if (!Way.cordinates.ContainsKey(key) || Way.cordinates[this.x + "-" + this.z].name != "way")
        {
            Way.cordinates.Add(this.x + "-" + this.z, new WayObject(way, "way", 2, 0, this.x, this.z, direction));
        }
        else
        {
            Way.cordinates[key].type = Way.cordinates[key].type + 1;
            Way.cordinates[key].wayDirections.Add(direction);
            Way.openBranchs.Add(Way.cordinates[key]);
        }




        this.z = z;
        this.x = x;
        this.stepSize = stepSize;

    }

}


