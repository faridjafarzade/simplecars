﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LoadingPage : MonoBehaviour {

    public Text loadingText;

    IEnumerator Start() {
        print("loading");
            DBAccess db = new DBAccess();
        UnityWebRequest download = db.getUser();

        yield return download.SendWebRequest();
        if (download.isNetworkError || download.isHttpError)
        {
            loadingText.text="Error : " + download.error;
        }
        else
        {
           
           
            Debug.Log(download.downloadHandler.text);
            MyRespons mr = MyRespons.CreateFromJSON(download.downloadHandler.text);
            Debug.Log(mr.user.userName);
            Game.user = mr.user;
            SceneManager.LoadScene("Menu");
        }
    }
	
	
}
