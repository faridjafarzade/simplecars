﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour {

    public static float x = 0, y = 0, z = 0;

    public bool moveRight = false;
    public bool moveLeft = false;
    public bool moveForward = false;
    public bool moveBack = false;
    public bool zoomIn = false;
    public bool zoomOut = false;

    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate() {

        if (moveRight)
        {
            x++;
          }

        if (moveLeft)
        {
            x--;
        }

        if (moveForward)
        {
            z++;
        }

        if (moveBack)
        {
            z--;
        }

        if (zoomIn)
        {
            if (y < 900) y++;
            print("zoom " + y);
        }

        if (zoomOut)
        {
            if (y > 10) y--;
        }

        transform.localPosition =  new Vector3((float)x, (float)y, (float)z);

    }

    public void moveForwardUp() { moveForward = false; }
    public void moveForwardDown() { moveForward = true; }
    public void moveBackUp() { moveBack = false; }
    public void moveBackDown() { moveBack = true; }
    public void moveRightUp() { moveRight = false; }
    public void moveRightDown() { moveRight = true; }
    public void moveLeftUp() { moveLeft = false; }
    public void moveLeftDown() { moveLeft = true; }
    public void zoomInUp() { zoomIn = false; }
    public void zoomInDown() { zoomIn = true; }
    public void zoomOutUp() { zoomOut = false; }
    public void zoomOutDown() { zoomOut = true; }
}
