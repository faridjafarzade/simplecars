﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CamerasController : MonoBehaviour {

    public static Camera currentCamera;
    public static Camera rightCamera;
    public static Camera leftCamera;
    public static Camera frontCamera;
    public static Camera backCamera;
    public static Camera mapCamera;

    public Camera staticCurrentCamera;
    public Camera staticRightCamera;
    public Camera staticLeftCamera;
    public Camera staticFrontCamera;
    public Camera staticBackCamera;
    public Camera staticMapCamera;

    public Button rightButton;
    public Button leftButton;
    public Button upButton;
    public Button downButton;

    public Button rightButtonMap;
    public Button leftButtonMap;
    public Button upButtonMap;
    public Button downButtonMap;
    public Button zoomInButtonMap;
    public Button zoomOutButtonMap;

    private static Text staticCountText;
    private static Text staticWinText;
    private static Text staticTimerText;


    private Text countText;
    private Text winText;
    private Text timerText;

    void Start()
    {

       staticCurrentCamera = currentCamera;
       staticRightCamera = rightCamera;
       staticLeftCamera = leftCamera;
       staticFrontCamera = frontCamera;
       staticBackCamera = backCamera;
    staticMapCamera = mapCamera;


    rightCamera.enabled = false;
        leftCamera.enabled = false;
        frontCamera.enabled = false;
        backCamera.enabled = false;

        currentCamera = backCamera;
        mapCamera.enabled = true;

    }



    public static void SetCountText(int coinCount)
    {
        CamerasController.staticCountText.text = "Count: " + coinCount.ToString();

    }

    void turnOffCameras()
    {
        rightCamera.enabled = false;
        leftCamera.enabled = false;
        frontCamera.enabled = false;
        backCamera.enabled = false;

    }
    void turnOnFrontCamera()
    {
        turnOffCameras();
        currentCamera = frontCamera;
        currentCamera.enabled = true;

    }
    void turnOnBackCamera()
    {
        turnOffCameras();
        currentCamera = backCamera;
        currentCamera.enabled = true;

    }
    void turnOnRightCamera()
    {
        turnOffCameras();
        currentCamera = rightCamera;
        currentCamera.enabled = true;

    }
    void turnOnLeftCamera()
    {
        turnOffCameras();
        currentCamera = leftCamera;
        currentCamera.enabled = true;

    }

    public void turnMap(bool map)
    {
        rightButton.enabled = !map;
        leftButton.enabled = !map;
        upButton.enabled = !map;
        downButton.enabled = !map;

        rightButtonMap.enabled = map;
        leftButtonMap.enabled = map;
        upButtonMap.enabled = map;
        downButtonMap.enabled = map;
        zoomInButtonMap.enabled = map;
        zoomInButtonMap.enabled = map;
    }



}
