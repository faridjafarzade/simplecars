﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WayObjectManager
{


    static public void addFinish(WayObject wo)
    {
        
        wo.go.GetComponent<Renderer>().material = ThemaManager.currentThema.finishMaterial;


        GameObject finish = GameObject.Instantiate(wo.go, new Vector3(wo.x * 10, -9f, wo.z * 10), Quaternion.identity);
        finish.tag = "finish";
        finish.GetComponent<Renderer>().material = ThemaManager.currentThema.finishMaterial;
        finish.transform.localScale += new Vector3(3, 3, 3);
        finish.transform.Rotate(0, 90-wo.rotation, 90);


    }

}

