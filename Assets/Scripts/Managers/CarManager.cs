﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarManager : MonoBehaviour
{


    public GameObject blueCar;
    public GameObject superBus;
    public static GameObject activeCar;
    private static int xDifference = 0;
    private static int zDifference = 0;
    public static List<Car> cars = new List<Car>();
    Image image;
    public static Car activeCar_;



    void Start(){

        cars.Add(new Car("super bus",0, superBus, 1, 0.01));
        cars.Add(new Car("blue car", 1, blueCar, 2, 0.1));
        cars.Add(new Car("blue car2", 2, blueCar, 3, 0.1));
        Debug.Log("caaaarrr  "+Game.car);
        activeCar_ = cars[Game.car];

        CarManager.activeCar = GameObject.Instantiate(activeCar_.gm, new Vector3(0, 0, 0), Quaternion.identity);

        CarManager.activeCar.SetActive(true);
    }

    public static void desingCars() {

        if (CarManager.cars.Count == 0)
        {
            cars.Add(new Car("blue car 1", 0, 1, 0.2));
            cars.Add(new Car("blue car 2",1, 2, 0.3));
            cars.Add(new Car("blue car 3", 1, 3, 0.3));

        }
    }

    public static void setCar(GameObject car)
    {
        activeCar_ = cars[Game.car];
        CarManager.activeCar = GameObject.Instantiate(car, new Vector3(0, 0, 0), Quaternion.identity);
        CarManager.activeCar.SetActive(true);
    }

    public static void changePosition(double x,double z)
    {

        CarManager.activeCar.transform.localPosition = new Vector3((float)x + CarManager.xDifference, 0.45f, (float)z + CarManager.zDifference);

    }

    public static void rotate(GameObject go, double rotate)
    {
        CarManager.activeCar.transform.RotateAround(go.transform.localPosition, go.transform.up, (float)rotate);

        //CarManager.activeCar.transform.rotation = Quaternion.Lerp(go.transform.rotation, Quaternion.Euler(new Vector3(0f, (float)rotate, 0f)), Time.fixedDeltaTime);

        //CarManager.activeCar.transform.RotateAround(Vector3.zero, Vector3.up, (float)rotate);
        //CarManager.activeCar.transform.RotateAround(Vector3.zero, Vector3.up, 20 * Time.deltaTime);
        // CarManager.activeCar.transform.Rotate((float)rotate, 0, 0);

    }

    public static void rotate(double rotate)
    {
        //CarManager.activeCar.transform.rotation = Quaternion.Euler(0f, (float)rotate, 0f);
        // CarManager.activeCar.transform.Rotate(0, (float)rotate, 0);
        CarManager.activeCar.transform.eulerAngles = new Vector3(0, (float)rotate, 0);
        //CarManager.activeCar.transform.Rotate(new Vector3(0, (float)rotate, 0) );
    }


}
