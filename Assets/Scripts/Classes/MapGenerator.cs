﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator {


    static private float maxX = 0;
    static private float maxZ = 0;
    static private float minX = 100000000000000000;
    static private float minZ = 100000000000000000;

    public MapGenerator()
    {

        MapGenerator.minX = 100000000000000000;
        MapGenerator.minZ = 100000000000000000;

    }

    public static Camera generate(Camera mapCamera)
    {
        CarScript.myPrint("map maxX-" + maxX + "  maxZ-" + maxZ);
        CarScript.myPrint("map minX-" + minX + "  minZ-" + minZ);
        float c = 10;
        float x = (maxX * c + minX * c) / 2;
        float z = (maxZ * c + minZ * c) / 2;
        //float y = (x + z) / 2;
        float xh = maxX - minX;
        float zh = maxZ - minZ;
        float xzh = zh * xh;
        //double y = Mathf.Sqrt(xzh);
        float y = 0;
        if (xzh > 900) y = 400.0f;
        else if(xzh > 700) y = 350.0f;
        else if (xzh > 300) y = 300.0f;
        else if (xzh > 200) y = 150.0f;
        else if (xzh > 100) y = 100.0f;
        else y = 50.0f;
        //float my = xh > zh ? xh : zh;
        MapController.x = x;
        MapController.y = y;
        MapController.z = z;

        CarScript.myPrint("map x-" + x + " y-" + y + " xzh-" + xzh);
        mapCamera.transform.localPosition = new Vector3(x, y, z);
        return mapCamera;

    }

    public static void generate()
    {
        CarScript.myPrint("map maxX-" + maxX + "  maxZ-" + maxZ);
        CarScript.myPrint("map minX-" + minX + "  minZ-" + minZ);
        float c = 10;
        float x = (maxX * c + minX * c) / 2;
        float z = (maxZ * c + minZ * c) / 2;
        //float y = (x + z) / 2;
        float xh = maxX - minX;
        float zh = maxZ - minZ;
        float xzh = zh * xh;
        //double y = Mathf.Sqrt(xzh);
        float y = 0;
        if (xzh > 900) y = 400.0f;
        else if (xzh > 700) y = 350.0f;
        else if (xzh > 300) y = 300.0f;
        else if (xzh > 200) y = 150.0f;
        else if (xzh > 100) y = 100.0f;
        else y = 50.0f;
        //float my = xh > zh ? xh : zh;
        MapController.x = x;
        MapController.y = y;
        MapController.z = z;

        CarScript.myPrint("map x-" + x + " y-" + y + " xzh-" + xzh);
      //  mapCamera.transform.localPosition = new Vector3(x, y, z);
       

    }


    public static void setCordinates(float x, float z)
    {
        if (x > MapGenerator.maxX) MapGenerator.maxX = x;
        if (x < MapGenerator.minX) MapGenerator.minX = x;
        if (z > MapGenerator.maxZ) MapGenerator.maxZ = z;
        if (z < MapGenerator.minZ) MapGenerator.minZ = z;
    }

}
