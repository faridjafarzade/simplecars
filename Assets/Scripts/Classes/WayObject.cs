﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayObject
{
    public GameObject go { get; set; }

    public GameObject downgo { get; set; }

    public float x { get; set; }
    public float z { get; set; }
    public int type { get; set; }
    public string name { get; set; }
    public float rotation { get; set; }
    public List<string> wayDirections = new List<string>();
    public WayObject()
    {

        this.name = "empty";

    }


    public WayObject(GameObject go, string name, int type, float rotation, float x, float z, string directions)
    {

        this.go = go;


        this.z = z;
        this.x = x;
        this.rotation = rotation;
        this.wayDirections.Add(directions);
        this.name = name;
        this.type = type;

    }



    public void generate()
    {
        //this.downgo = GameObject.Instantiate(this.go, new Vector3(x * 10, -50.5f, z * 10), Quaternion.identity);
        //this.downgo.tag = "inway";
        GameObject wgo = GameObject.FindWithTag(ThemaManager.currentThema.way);
        this.go = GameObject.Instantiate(wgo, new Vector3(x * 10, -0.2f, z * 10), Quaternion.identity);
        this.go.tag = "inway";
       

        CarScript.myPrint("rotation " + this.rotation);
        this.rotateX(90);
        this.rotateZ(this.rotation);
        this.scale(3, 8, 2);
         float mx = x - 1;
        float px = x + 1;
        float mz = z - 1;
        float pz = z + 1;

        this.prepare(mx, z);
        this.prepare(x, mz);
        this.prepare(mx, mz);
        this.prepare(px, z);
        this.prepare(x, pz);
        this.prepare(px, pz);

        int c = Random.Range(0, 10);
        if (c == 1)
        {
            GameObject coin_ = GameObject.Instantiate(ThemaManager.staticCoin, new Vector3(x * 10, 0.5f, z * 10), Quaternion.identity);
            coin_.transform.localScale += new Vector3(2, 2, 2);
        }

    }


    public void prepare(float x, float z)
    {
        if (!Way.cordinates.ContainsKey(x + "-" + z))
        {
            GameObject so = ThemaManager.currentThema.sideObjects[Random.Range(0, ThemaManager.currentThema.sideObjects.Count)];
            GameObject.Instantiate(so, new Vector3(x * 10, 0, z * 10), Quaternion.identity).transform.localScale += new Vector3(2, 2, 2);
            GameObject.Instantiate(ThemaManager.currentThema.ground, new Vector3(x * 10, -0f, z * 10), Quaternion.identity);
            //GameObject.Instantiate(ThemaManager.currentThema.ground, new Vector3(x * 10, -25, z * 10), Quaternion.identity).tag = "outway";
        }
    }


    public void rotate(float degree)
    {
        this.go.transform.Rotate(0, 0, degree);
       // this.downgo.transform.Rotate(0, 0, degree);
    }

    public void rotateX(float degree)
    {
        this.go.transform.Rotate(degree, 0, 0);
      //  this.downgo.transform.Rotate(degree, 0, 0);
    }

    public void rotateZ(float degree)
    {
        this.go.transform.Rotate(0, 0, degree);
       // this.downgo.transform.Rotate(0, 0, degree);
    }

    public void rotateY(float degree)
    {
        this.go.transform.Rotate(0, degree, 0);
       // this.downgo.transform.Rotate(0, degree, 0);
    }



    public void scale(float tx, float ty, float tz)
    {
        this.go.transform.localScale += new Vector3(tx, ty, tz);
      //  this.downgo.transform.localScale += new Vector3(tx, ty, tz);

    }

}
