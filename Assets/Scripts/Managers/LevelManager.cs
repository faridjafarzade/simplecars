﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager  {

    public static List<Level> levels = new List<Level>();
    public static Level currentLevel;

	public LevelManager(int id) {
        designLevels();
        currentLevel = levels[id];
    }

    public LevelManager()
    {
        designLevels();
        currentLevel = levels[0];
    }

    public static void designLevels()
    {   
        if(levels.Count == 0) {
        levels.Add(new Level("level 1 ",30,2,10,60, 5, 45,0,null,null,null,null));
        levels.Add(new Level("level 2 ", 50,4, 20,30,10, 30, 1, null, null, null, null));
        }
    }

    public static Level getCurrentLevel(int id)
    {
        designLevels();
        return levels[id];
    }
}
